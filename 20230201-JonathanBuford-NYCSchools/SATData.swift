//
//  SATData.swift
//  20230201-JonathanBuford-NYCSchools
//
//  Created by Jonathan Buford on 2/11/23.
//

import Foundation

struct SATData: Decodable {
    
    // MARK: - Properties
    
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
