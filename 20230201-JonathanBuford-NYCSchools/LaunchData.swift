//
//  LaunchData.swift
//

import Foundation

struct LaunchData: Codable {
    
    // MARK: - Properties
    
    let dbn: String
    let school_name: String
    let overview_paragraph: String?
    let academicopportunities1: String?
    let academicopportunities2: String?
    let phone_number: String?
    let fax_number: String?
    let school_email: String?
    let website: String?
    let finalgrades: String?
    let total_students: String?
    let school_sports: String?
    let attendance_rate: String?
    let latitude: String?
    let longitude: String?
    let location: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}
