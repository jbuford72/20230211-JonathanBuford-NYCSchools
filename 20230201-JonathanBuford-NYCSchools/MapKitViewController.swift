//
//  MapKitViewController.swift
//  20230201-JonathanBuford-NYCSchools
//
//  Created by Jonathan Buford on 1/31/23.
//

import UIKit
import MapKit
import CoreLocation
import Foundation

class MapKitViewController: UIViewController {
    
    @IBOutlet weak var mapKitOutlet: MKMapView!
    
    private let locationManager = CLLocationManager()
    private var currentCoordinate: CLLocationCoordinate2D?
    private var holdingSchoolData: [LaunchData]?
    private var holdingSATData: [SATData]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.configureLocationServices(_:)),
                                               name: NSNotification.Name(rawValue: "schoolDataReceived"),
                                               object: nil)
        NetworkTraffic.shared.handleNetworkCalls()
    }
    
    @objc private func configureLocationServices(_ notification: NSNotification) {
        mapKitOutlet.delegate = self

        if let passSchoolData = notification.userInfo?["schools"] as? [LaunchData] {
            holdingSchoolData = passSchoolData
            print(passSchoolData[3].school_name)
        }

        let status = locationManager.authorizationStatus
        
        if  status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            mapKitOutlet.mapType = .standard
            mapKitOutlet.isZoomEnabled = true
            mapKitOutlet.isScrollEnabled = true
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
            beginLocationUpdates(locationManager: locationManager)
            addAnnotations()
        }
    }
    
    private func zoomToLatestLocation(with coordinate: CLLocationCoordinate2D) {
        let zoomRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: GlobalVariables.latitudinalMeters, longitudinalMeters: GlobalVariables.longitudinalMeters)
        mapKitOutlet.setRegion(zoomRegion, animated: true)
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapKitOutlet.showsUserLocation = true
        
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        /// Setting hundred meters at this point because I am not interested
        /// setting up for Navigation at this time.  This is something that can be
        /// explored later.
        
        locationManager.startUpdatingLocation()
        /// I am starting the location updating service for now in spite of the fact that
        /// this test app will probably be ran on Simulator.  It is the right time to do it
        /// here if we were to be planning for running this app on a physical device.
    }
    
    private func addAnnotations() {
        if let schoolData = holdingSchoolData {
            for school in schoolData as [LaunchData] {
                let newAnnotation = MKPointAnnotation()
                newAnnotation.subtitle = school.school_name
                if let schoolLat = school.latitude {
                    if let schoolLong = school.longitude {
                        /// Unwrapping the school coordinates to convert them to type Double.
                        ///  If they are empty for any reason, we can catch them and do something
                        ///  with them.
                        newAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(schoolLat)!,
                                                                          longitude: Double(schoolLong)!)
                    } else {
                        print("\(school.school_name) has no Latitude entry!")
                    }} else {
                        print("\(school.school_name) has no Longitude entry!")
                    }
                mapKitOutlet.addAnnotation(newAnnotation)
            }
        }
    }
}

extension MapKitViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let latestLocation = locations.first else { return }
        if currentCoordinate == nil {
            zoomToLatestLocation(with: latestLocation.coordinate)
        } else if mapKitOutlet.userLocation.coordinate.longitude == nil {
//            print("User Location not set")
        }
        currentCoordinate = latestLocation.coordinate
        
    }

    internal func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        /// Use of original locationManager(_:didChangeAuthorization:) was deprecated in iOS 14.0
        
        let status = manager.authorizationStatus
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        beginLocationUpdates(locationManager: manager)
    }
}

extension MapKitViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        if annotationView == nil {
            /// Instantiate the view if it's not already there.
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        if annotation === mapView.userLocation {
            /// This would be the user's location.
            /// I have set the user location to NYC, New York.  I will load the
            /// school location pins from there. Basically, I am doing this under the
            /// assumption that this app will be ran on simulator for testing.
            if let annotationDiscovered = annotationView {
                annotationDiscovered.image = GlobalVariables.userLocationIconImage
                //            location = CLLocationCoordinate2D(latitude: 40.774300, longitude: -73.981194)
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: 40.774300, longitude: -73.981194)
            }
            mapView.addAnnotation(annotation)
            zoomToLatestLocation(with: annotation.coordinate)
        } else {
             /// Here is where I will add the image for each school pins.
            if let schoolAnnotationDiscovered = annotationView {
                schoolAnnotationDiscovered.image = GlobalVariables.schoolLocationIconImage
            }
            mapView.addAnnotation(annotation)
        }
        annotationView?.canShowCallout = true
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("Annotation selected: \(String(describing: view.annotation?.subtitle))")
        for school in holdingSchoolData! {
            /// Using explicit "holdingSchoolData" reference here because
            /// the app would not get to this point if this variable was empty.
            
            if view.annotation?.subtitle == school.school_name {
                prepareAlertView(view: view, school: school)
            }
        }
    }
    
}

extension MapKitViewController: UIAlertViewDelegate {
    private func prepareAlertView(view: MKAnnotationView, school:LaunchData) {
        if let annotationSubTitle = view.annotation?.subtitle {
            /// Using the annotation subtitle to get the school name rather than the dictionary
            /// to make sure the school name was initially put in place.
            if let annotationAddress = school.location {
                let schoolSummaryActionSheet =  UIAlertController(title: annotationSubTitle,
                                                                  message: annotationAddress,
                                                                  preferredStyle: UIAlertController.Style.actionSheet)
                schoolSummaryActionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                schoolSummaryActionSheet.addAction(UIAlertAction(title: "Detail View", style: UIAlertAction.Style.default, handler: { (ACTION :UIAlertAction!)in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "detailView") as! SchoolDetailViewController
                    GlobalVariables.shared.schoolData = school
                    self.present(vc, animated: true)
                    }))
                self.present(schoolSummaryActionSheet, animated: true, completion: nil)
            }
        }
    }
}
