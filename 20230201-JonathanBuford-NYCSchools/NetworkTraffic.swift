//
//  NetworkTraffic.swift
//  GlobalPaymentsTest
//
//  Created by Jonathan Buford on 11/20/22.
//

import UIKit
import Foundation

class NetworkTraffic: NSObject {
    var decodedSATstats:[SATData]?
    var decodedSchoolData: [LaunchData]?
    let group = DispatchGroup()

    static let shared = NetworkTraffic()
    
    override init(){}

    func gatherSchoolData() {
        
        let requestString = URL(string: GlobalVariables.networkSchoolURL)!
        var request = URLRequest(url: requestString)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                if let launches = try! decoder.decode([LaunchData]?.self, from: data) {
                    print("Valid response for School data received and decoded.")
                    self.decodedSchoolData = launches
                    DispatchQueue.main.async {
                        print("School GCD group")
                        self.group.leave()   // <<----
                    }
                } else {
                    print("Invalid response recieved for School data.")
                }
            } else if let error = error {
                print("HTTP Request (School) Failed \(error)")
            }
        }
        task.resume()

    }
    
    func gatherSATStats() {
        let requestString = URL(string: GlobalVariables.networkSATUrl)!
        var request = URLRequest(url: requestString)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                if let decSATstats = try! decoder.decode([SATData]?.self, from: data) {
                    print("Valid response for SAT data received and decoded.")
                    self.decodedSATstats = decSATstats
                    DispatchQueue.main.async {
                        print("SAT GCD group")
                        self.group.leave()   // <<----
                    }
                } else {
                    print("Invalid response recieved for SAT data.")
                }
            } else if let error = error {
                print("HTTP Request (SAT) Failed \(error)")
            }
        }
        task.resume()
    }
    
    func combineData() {
        if let decSchoolData = decodedSchoolData {
            if let decSATdata = decodedSATstats {
                for (index, _) in decSchoolData.enumerated() {
                    for satDatForSchool in decSATdata {
                        if decodedSchoolData![index].dbn == satDatForSchool.dbn {
                            decodedSchoolData![index].num_of_sat_test_takers = satDatForSchool.num_of_sat_test_takers
                            decodedSchoolData![index].sat_critical_reading_avg_score = satDatForSchool.sat_critical_reading_avg_score
                            decodedSchoolData![index].sat_math_avg_score = satDatForSchool.sat_math_avg_score
                            decodedSchoolData![index].sat_writing_avg_score = satDatForSchool.sat_writing_avg_score
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.group.leave()   // <<----
                }
            }
        }
    }
    
    func handleNetworkCalls() {
        group.enter()
        self.gatherSchoolData()

        group.enter()
        self.gatherSATStats()

        group.notify(queue: .main) {
            self.group.enter()
            self.combineData()

            self.group.notify(queue: .main) {
                let passLaunchData:[String: [LaunchData]?] = ["schools": self.decodedSchoolData]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "schoolDataReceived"),
                                                object: nil,
                                                userInfo: passLaunchData as [AnyHashable : Any])
            }
        }
    }
}
