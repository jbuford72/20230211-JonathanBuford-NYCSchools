//
//  SchoolDetailView.swift
//  20230201-JonathanBuford-NYCSchools
//
//  Created by Jonathan Buford on 2/8/23.
//

import UIKit

class SchoolDetailView: UIView {

    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phonePrompt: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var faxPrompt: UILabel!
    @IBOutlet weak var fax: UILabel!
    @IBOutlet weak var emailPrompt: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var websitePrompt: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var finalGradesPrompt: UILabel!
    @IBOutlet weak var finalGrades: UILabel!
    @IBOutlet weak var totalStudentsPrompt: UILabel!
    @IBOutlet weak var totalStudents: UILabel!
    @IBOutlet weak var attendanceRatePrompt: UILabel!
    @IBOutlet weak var attendanceRate: UILabel!
    @IBOutlet weak var accademicOpportunitiesPrompt: UILabel!
    @IBOutlet weak var accademicOpportunities1: UILabel!
    @IBOutlet weak var accademicOpportunities2: UILabel!
    @IBOutlet weak var schoolSportsPrompt: UILabel!
    @IBOutlet weak var schoolSports: UILabel!
    @IBOutlet weak var satCriticalReading: UILabel!
    @IBOutlet weak var satMath: UILabel!
    @IBOutlet weak var satWriting: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        let schoolData = GlobalVariables.shared.schoolData
        
        if let name = schoolData?.school_name {
            self.schoolName.text = name
        } else {
            self.schoolName.isHidden = true
        }

        if let overview = schoolData?.overview_paragraph {
            self.overview.text = overview
        } else {
            self.overview.isHidden = true
        }

        if let schoolAddress = schoolData?.location {
            self.address.text = schoolAddress
        } else {
            self.address.isHidden = true
        }
        
        if let schoolPhone = schoolData?.phone_number {
            self.phone.text = schoolPhone
        } else {
            self.phone.isHidden = true
            self.phonePrompt.isHidden = true
        }

        if let schoolFax = schoolData?.fax_number {
            self.fax.text = schoolFax
        } else {
            self.fax.isHidden = true
            self.faxPrompt.isHidden = true
        }

        if let schoolEmail = schoolData?.school_email {
            self.email.text = schoolEmail
        } else {
            self.email.isHidden = true
            self.emailPrompt.isHidden = true
        }

        if let schoolWebsite = schoolData?.website {
            self.website.text = schoolWebsite
        } else {
            self.website.isHidden = true
            self.websitePrompt.isHidden = true
        }
        
        if let schoolFinalGrades = schoolData?.finalgrades {
            self.finalGrades.text = schoolFinalGrades
        } else {
            self.finalGrades.isHidden = true
            self.finalGradesPrompt.isHidden = true
        }

        if let schoolTotalStudent = schoolData?.total_students {
            self.totalStudents.text = schoolTotalStudent
        } else {
            self.totalStudents.isHidden = true
            self.totalStudentsPrompt.isHidden = true
        }

        if let schoolAttendance = schoolData?.finalgrades {
            self.attendanceRate.text = schoolAttendance
        } else {
            self.attendanceRate.isHidden = true
            self.attendanceRatePrompt.isHidden = true
        }
        
        if let schoolAcOp = schoolData?.academicopportunities1 {
            self.accademicOpportunities1.text = schoolAcOp
        } else {
            self.accademicOpportunities1.isHidden = true
            self.accademicOpportunities2.isHidden = true
            self.accademicOpportunitiesPrompt.isHidden = true
        }

        if let schoolAcOp2 = schoolData?.finalgrades {
            self.accademicOpportunities2.text = schoolAcOp2
        } else {
            self.accademicOpportunities2.isHidden = true
        }

        if let sports = schoolData?.school_sports {
            self.schoolSports.text = sports
        } else {
            self.schoolSports.isHidden = true
            self.schoolSportsPrompt.isHidden = true
        }
        
        if let satCritRead = schoolData?.sat_critical_reading_avg_score {
            self.satCriticalReading.text = satCritRead
        } else {
            self.satCriticalReading.isHidden = true
        }

        if let satAvgMath = schoolData?.sat_math_avg_score {
            self.satMath.text = satAvgMath
        } else {
            self.satMath.isHidden = true
        }

        if let satAvgWriting = schoolData?.sat_writing_avg_score {
            self.satWriting.text = satAvgWriting
        } else {
            self.satWriting.isHidden = true
        }
    }
}
