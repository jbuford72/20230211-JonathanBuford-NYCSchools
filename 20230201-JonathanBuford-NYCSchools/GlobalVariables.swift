//
//  GlobalVariables.swift
//  20230201-JonathanBuford-NYCSchools
//
//  Created by Jonathan Buford on 1/29/23.
//

import UIKit

class GlobalVariables: NSObject {
    static let networkSchoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let networkSATUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static let latitudinalMeters: Double = 10000 /// Added here for ease of changing default value.
    static let longitudinalMeters: Double = 10000 /// Added here for ease of changing default value.
    static let schoolLocationIconImage: UIImage = UIImage(named: "school")!
    /// I know this is safe to be treated as explicit as I have set the name value myself
    /// and it will always return an image.
    static let userLocationIconImage: UIImage = UIImage(systemName: "person.crop.circle.fill")!
    /// I know this is safe to be treated as explicit as is a system symbol
    /// and it will always return an image.
    
    var schoolData: LaunchData?
        
    static let shared = GlobalVariables()
    
    override init(){}
}
